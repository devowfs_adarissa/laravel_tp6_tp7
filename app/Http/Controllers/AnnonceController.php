<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Http\Requests\AnnonceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $annonces=Annonce::all();
       return view("annonce.index", compact("annonces"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("annonce.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AnnonceRequest $request)
    {   
        $data=$request->all();
        if($request->has("photo")){
              $photo=$request->photo;
              $nameOfPhoto=$photo->store("photos");
              $data["photo"]= $nameOfPhoto;
        }
      

       Annonce::create($data);
       return redirect()->route("annonce.index")->with("success", "L'ajout de la nouvelle annonce a bien réussi!");
    }

    /**
     * Display the specified resource.
     */
    public function show(Annonce $annonce)
    {
       return view("annonce.show", compact("annonce"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Annonce $annonce)
    {
        return view("annonce.edit", compact("annonce"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AnnonceRequest $request, Annonce $annonce)
    {
        $data=$request->all();
        if($request->has("photo")){
            $photo=$request->photo;
            $nameOfPhoto=$photo->store("photos");

            $data["photo"]= $nameOfPhoto;
        }
        else{
            $data["photo"]=$annonce->photo;
        }

        $annonce->update($data);
         return redirect()->route("annonce.index")->with("success", "La modification a bien réussi!");

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Annonce $annonce)
    {
           $annonce->delete();
           return redirect()->route("annonce.index")->with("success", "La suppression a bien réussi!");

    }
}
