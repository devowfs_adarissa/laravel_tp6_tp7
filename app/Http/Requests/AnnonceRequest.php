<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AnnonceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            "titre"=>["required",
            Rule::unique("annonces")->ignore($this->annonce)],
            "decription"=>"max:100",
            "type"=>"in:Appartement,Maison,Terrain,Villa,Magasin",
            "decription"=>"alpha",
            "superficie"=>"integer|min:20",
            "prix"=>"numeric|min:0",
            "photo"=>"image|mimes:png,jpeg,jpg"
           
        ];
    }
    public function messages()
    {   
        return [
            "titre.required"=>"Le titre est obligatoire",
            "titre.unique"=>"Ce titre est déjà pris!",
            "decription.max"=>"la taille max de la description est 100 cara",
            "photo.mimes"=>"Le format de la photo est invalide",
            "photo.image"=>"Veuillez choisir une image",
            

           
           
        ];
        
    }
}
