@extends("template.master")
@section("titre", "Liste des annonces")
@section("contenu")
    @isset($annonce)
        <h2>Détails de l'annonce numéro {{ $annonce->id }}</h2>
             @if(isset($annonce->photo))
                            <img src="{{ asset($annonce->photo) }}" alt="" width="500px">
                        @else
                            <img src="{{ asset('photos/default.png') }}" alt="" width="500px">
                        @endif <br>
        Titre:  {{ $annonce->titre }} <br>
        Description:  {{ $annonce->description }} <br>
        Etat:  {{ $annonce->neuf? "Neuf":"Ancien" }}
    @endisset
@endsection