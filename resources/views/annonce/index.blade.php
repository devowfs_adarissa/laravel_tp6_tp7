@extends("template.master")
@section("titre", "Liste des annonces")
@section("contenu")
<h2 class="mt-2">Liste des annonces</h2>
<a href="{{ route('annonce.create') }}" class="btn btn-primary mt-2 mb-3">Nouvelle annonce</a>
    @if(session()->has("success"))    
    <div class="alert alert-success">
        {{session("success")}}
    </div>  
    @endif
@isset($annonces)
        <table class="table">
            <tr>
                <th>Photo</th>
                <th>Titre</th>
                <th>Description</th>
                <th>Type</th>
                <th>Ville</th>
                <th>Superficie (m<sup>2</sup>)</th>
                <th>Etat</th>
                <th>Prix (dhs)</th>
                <th>Actions</th>
            </tr>
            @foreach($annonces as $annonce)
                <tr>
                    <td>
                        @if(isset($annonce->photo))
                            <img src="{{ asset($annonce->photo) }}" alt="" width="80px">
                        @else
                            <img src="{{ asset('photos/default.png') }}" alt="" width="80px">
                        @endif
                    </td>
                    <td>{{ $annonce->titre }}</td>
                    <td>{{ $annonce->description }}</td>
                    <td>{{ $annonce->type }}</td>
                    <td>{{ $annonce->ville }}</td>
                    <td>{{ $annonce->superficie }}</td>
                    <td>{{ ($annonce->neuf)? "Neuf":"Ancien" }}</td>
                    <td>{{ $annonce->prix }}</td>
                    <td>
                        <form action="{{ route('annonce.destroy',  $annonce->id ) }}" method="post">
                            @csrf
                            @method("DELETE")
                            <a href="{{ route('annonce.edit',  $annonce->id ) }}" ><i class="bi bi-pencil text-success p-1"></i></a>
                            <a href="{{ route('annonce.show',  $annonce->id ) }}" > <i class="bi bi-eye text-secondary p-1"></i></a>
                            <button type="submit" class="btn border-none p-1"
                            onclick="return confirm('Voulez vous supprimer {{ $annonce->titre }}?')"
                            > <i class="bi bi-trash2 text-danger" style="font-size:20px"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
           
        </table>
    @endisset
@endsection